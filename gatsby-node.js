const path = require("path")

exports.createPages = ({ actions, graphql }) => {
  const { createPage } = actions

  return graphql(`
    {
      allWordpressPost {
        edges {
          node {
            id
            slug
            status
          }
        }
      }
    }
  `).then(result => {
    if (result.errors) {
      result.errors.forEach(e => console.error(e.toString()))
      return Promise.reject(result.errors)
    }

    const blogListTemplate = path.resolve(`./src/templates/blog-list.js`)
    createPage({
      path: `/`,
      component: blogListTemplate,
      context: {},
    })

    const blogPostTemplate = path.resolve("./src/templates/blog-post.js")
    const posts = result.data.allWordpressPost.edges
    posts.forEach(({ node: page }) => {
      createPage({
        path: `${page.slug}`,
        component: blogPostTemplate,
        context: {
          id: page.id,
        },
      })
    })
  })
}
