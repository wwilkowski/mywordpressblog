import React from "react"
import { graphql } from "gatsby"

const BlogPost = props => {
  const { title, date, content } = props.data.wordpressPost
  return (
    <div>
      <h1>{title}</h1>
      <p>{date}</p>
      <div dangerouslySetInnerHTML={{ __html: content }} />
    </div>
  )
}

export const pageQuery = graphql`
  query BlogPostByID($id: String!) {
    wordpressPost(id: { eq: $id }) {
      id
      title
      slug
      content
      date(formatString: "MMMM DD, YYYY")
    }
  }
`

export default BlogPost
