import React from "react"
import { graphql, Link } from "gatsby"
import Layout from "../components/layout"

class BlogList extends React.Component {
  render() {
    const { edges: posts } = this.props.data.allWordpressPost
    return (
      <Layout>
        <h1> Oto wszystkie moje wpisy! </h1>
        <ul>
          {posts.map(post => {
            const { id, title, content, slug } = post.node
            return (
              <li key={id}>
                <h2>{title}</h2>
                <Link to={`/${slug}`}>Czytaj więcej</Link>
              </li>
            )
          })}
        </ul>
      </Layout>
    )
  }
}

export default BlogList

export const allPagesQuery = graphql`
  query allPagesQuery {
    allWordpressPost {
      edges {
        node {
          id
          title
          slug
        }
      }
    }
  }
`
